
## if
```bash
if [[ ! -f package.json ]]; then
  echo "No package.json found! A package.json file is required to publish a package to GitLab's NPM registry."
  echo 'For more information, see https://docs.gitlab.com/ee/user/packages/npm_registry/#creating-a-project'
  exit 1
fi
```

## for
```bash
for WORD in "Hello" "World" "*" "Nice" "to" "meet" "you."; do
    echo "The word is: $WORD"
done
```

## case
```bash
case STRING in
  globPattern1)
    statements
  ;;
  globPattern2)
    statements
  ;;
esac
```