# Epic
- [ ] Enable users to edit the script. Reverse the script to generate the steps

# Feature List

- [x] Default components that apply to every step: image, before_script
- [x] Step name can be changed(Not step stage); and the change will be reflected to el-collapse
- [x] Let users choose their CI template(npm, maven)
  - [x] npm example: https://gitlab.com/pcfeng502/ci-generator/-/blob/master/.gitlab-ci.yml
- [x] Mock a test really using ci
- [ ] When a user expand a el-collapse item, the script corresponding to it should be highlighted
- [x] Give a tooltip hint when users move on to a step template
- [x] In each step, enable users to add/delete terms such as script or tags
- [x] Debug: If two build template was dragged onto steps, then these two will be opened together
- [x] For multi-line component such as script, trim the leading and trailing empty lines
- [ ] For multi-line commands separated by | in the script, figure out a way to generate it
  - [ ] Wrap following keywords into (A single line?)/(A yaml literal block style?)
    - [ ] judge keywords: if-fi, for-do-done, while-do-done, until-do-done, case-esac. 
  - [ ] generate by yaml literal block style
    - [ ] 从js-yaml/dumper.js的源代码来看，好像可以用STYLE_LITERAL这个来。不行的，那个STYLE_LITERAL是一个内部变量
      - [x] NOT WORKING: Seems lik can pass lineWidth to be -1 to set block style to be STYLE_LITERAL
- [x] Script font changed to a monospaced font: Using vue-codemirror
- [x] When user change the "Preset Template", warn the users that all of his changes will be discarded

# Internal Optimization

- [x] Extract template from GitlabCI.vue
- [ ] Enable an option to hide the Script to the right side