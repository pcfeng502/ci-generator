export const stepTplList = [
  {
    displayName: 'DisplayGlobalDefault',
    id: 0,
    name: 'default',
    ciimage: 'maven:3.3.9-jdk-8',
    cibefore_script: 'date\npwd',
    ciafter_script: 'date',
    ciretry: 2
  },
  {
    displayName: 'DisplayBuild',
    id: 1,
    name: 'build',
    cistage: 'build',
    ciscript: 'mvn --batch-mode --errors --fail-at-end --show-version-DinstallAtEnd=true -DdeployAtEnd=true verify\necho BUILD FINISHED',
    ciimage: 'maven:3.3.9-jdk-8'
  },
  {
    displayName: 'DisplayTest',
    id: 2,
    name: 'test',
    cistage: 'test',
    ciscript: 'mvn test',
    ciimage: 'maven:latest'
  },
  {
    displayName: 'DisplayDeploy',
    id: 3,
    name: 'deploy',
    cistage: 'deploy',
    ciscript: 'systemctl start yourService.service',
    citags: 'yourDeployMachine'
  }
]
