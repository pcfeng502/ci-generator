export const mavenTpl = [
  {
    displayName: 'DisplayGlobalDefault',
    id: 0,
    name: 'default',
    content: {
      ciimage: 'maven:3.3.9-jdk-8',
      ciretry: 2,
      displayName: 'DisplayGlobalDefault',
      id: 0,
      name: 'default'
    }
  },
  {
    displayName: 'DisplayBuild',
    id: 1,
    name: 'mavenBuild',
    content: {
      cistage: 'build',
      ciscript: 'mvn --batch-mode --errors --fail-at-end --show-version-DinstallAtEnd=true -DdeployAtEnd=true verify\necho BUILD FINISHED',
      displayName: 'DisplayBuild',
      id: 1,
      name: 'mavenBuild'
    }
  },
  {
    displayName: 'DisplayTest',
    id: 2,
    name: 'mavenTest',
    content: {
      cistage: 'test',
      ciscript: 'mvn test',
      displayName: 'DisplayTest',
      id: 2,
      name: 'mavenTest'
    }
  }
]

export const npmTpl = [
  {
    displayName: 'DisplayGlobalDefault',
    id: 0,
    name: 'default',
    content: {
      ciimage: 'node:latest',
      cibefore_script: `NPM_PACKAGE_NAME=$(node -p "require('./package.json').name")\nNPM_PACKAGE_VERSION=$(node -p "require('./package.json').version")`,
      displayName: 'DisplayGlobalDefault',
      id: 0,
      name: 'default'
    }
  },
  {
    displayName: 'DisplayBuild',
    id: 1,
    name: 'npmBuild',
    content: {
      cistage: 'build',
      ciscript: 'npm build',
      displayName: 'DisplayBuild',
      id: 1,
      name: 'npmBuild'
    }
  }
]

export const bashTpl = [
  {
    displayName: 'DisplayGlobalDefault',
    id: 0,
    name: 'default',
    content: {
      ciimage: 'busybox:latest',
      cibefore_script: `echo "Before script section"\necho "For example you might run an update here or install a build dependency"\necho "Or perhaps you might print out some debugging details"`,
      ciafter_script: `echo "After script section"\necho "For example you might do some cleanup here"`,
      displayName: 'DisplayGlobalDefault',
      id: 0,
      name: 'default'
    }
  },
  {
    displayName: 'DisplayBuild',
    id: 1,
    name: 'bashBuild',
    content: {
      cistage: 'build',
      ciscript: 'echo "Do your build here"',
      displayName: 'DisplayBuild',
      id: 1,
      name: 'bashBuild'
    }
  },
  {
    displayName: 'DisplayTest',
    id: 2,
    name: 'bashTest',
    content: {
      cistage: 'test',
      ciscript: 'echo "Do a test here"\necho "For example run a test suite"',
      displayName: 'DisplayTest',
      id: 2,
      name: 'bashTest'
    }
  },
  {
    displayName: 'DisplayDeploy',
    id: 3,
    name: 'bashDeploy',
    content: {
      cistage: 'deploy',
      ciscript: 'echo "Do your deploy here"',
      displayName: 'DisplayDeploy',
      id: 3,
      name: 'bashDeploy'
    }
  }
]

export const pythonTpl = [
  {
    displayName: 'DisplayGlobalDefault',
    id: 0,
    name: 'default',
    content: {
      ciimage: 'python:latest',
      cibefore_script: `python -V\npip install virtualenv\nvirtualenv venv\nsource venv/bin/activate`,
      displayName: 'DisplayGlobalDefault',
      id: 0,
      name: 'default'
    }
  },
  {
    displayName: 'DisplayTest',
    id: 1,
    name: 'pythonTest',
    content: {
      cistage: 'test',
      ciscript: 'python setup.py test\npip install tox flake8\ntox -e py36,flake8',
      displayName: 'DisplayTest',
      id: 1,
      name: 'pythonTest'
    }
  }
]
