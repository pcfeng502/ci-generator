# Gitlab CI Generator

> 这是一个极简的 .gitlab-ci.yml 生成器，主要用来协助用户快速配置 gitlab ci。

在线Demo: https://pcfeng502.gitlab.io/ci-generator

## 使用方法
1. 访问线上地址，并且选择你的构建工具，例如后端Java一般选择maven, 前端Javascript一般选择npm。
2. 可以使用拖拽编辑的能力，让系统来帮助你创建对应的yaml脚本。
3. (可选)可以使用[gitlab自带的lint工具](https://docs.gitlab.com/ee/ci/lint.html)来检查yaml是否正常工作。
4. 把yaml脚本放在项目的.gitlab-ci.yml文件中，来完成gitlab流水线配置。


## 开发：Build Setup

```bash
# 克隆项目
git clone https://gitlab.com/pcfeng502/ci-generator.git

# 进入项目目录
cd ci-generator

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装以来，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```

浏览器访问 [http://localhost:9528](http://localhost:9528)

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```

## 其它

```bash
# 预览发布环境效果
npm run preview

# 预览发布环境效果 + 静态资源分析
npm run preview -- --report

# 代码格式检查
npm run lint

# 代码格式检查并自动修复
npm run lint -- --fix
```

## Demo

![demo](https://pcfeng502.gitlab.io/ci-generator)

## 相关文档

- [Gitlab CI/CD 流水线配置文档](https://docs.gitlab.com/ee/ci/yaml/)

- [Gitlab CI 语法检查工具](https://docs.gitlab.com/ee/ci/lint.html)


## License

[MIT](https://gitlab.com/pcfeng502/ci-generator/-/blob/master/LICENSE) license.