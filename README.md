# Gitlab CI Generator

English | [简体中文](./README-zh.md)

> A minimal .gitlab-ci.yml generator for gitlab-ci bootstrapping through an easy-to-understand, drag and drop GUI.

> It can also be used for learning gitlab-ci.yml configuration

**Live demo:** https://pcfeng502.gitlab.io/ci-generator

## Usage introduction

1. Visit the demo site and choose your build tool by setting **Preset Template** (such as maven, npm, bash).
2. Play with the drag-and-drop and let the system generate the yaml for you.
3. (Optional)Put the yaml inside [Gitlab CI lint tool](https://docs.gitlab.com/ee/ci/lint.html) to see if it is working.
4. Put the yaml file in .gitlab-ci.yml to enable the gitlab pipeline.

## Build Setup

```bash
# clone the project
git clone https://gitlab.com/pcfeng502/ci-generator.git

# enter the project directory
cd ci-generator

# install dependency
npm install

# develop
npm run dev
```

This will automatically open http://localhost:9528

## Build

```bash
# build for test environment
npm run build:stage

# build for production environment
npm run build:prod
```

## Advanced

```bash
# preview the release environment effect
npm run preview

# preview the release environment effect + static resource analysis
npm run preview -- --report

# code format check
npm run lint

# code format check and auto fix
npm run lint -- --fix
```


## Demo

[demo](https://pcfeng502.gitlab.io/ci-generator)


## Related Document

- [Gitlab CI/CD pipeline configuration reference](https://docs.gitlab.com/ee/ci/yaml/)

- [Gitlab CI lint tool](https://docs.gitlab.com/ee/ci/lint.html)


## License

[MIT](https://gitlab.com/pcfeng502/ci-generator/-/blob/master/LICENSE) license.